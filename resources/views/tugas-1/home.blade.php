@extends('layout.layout')

@section('judul')
    Profile
@endsection

@section('page')
    profile
@endsection

@section('content')
  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h1 class="card-title">MaulCompany</h1>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
          <h3>Selamat datang {{$firstname}} {{$lastname}}</h3>
          <p>jenis kelamin: 
              @if ($gender === "1")
                  Laki - Laki
              @elseif ($gender === "2")
                  Perempuan
              @else
                  Other
              @endif
          </p>
          <p>kewarganegaran: 
              @if($nationality === "1")
                  indonesia
              @elseif($nationality === "2")
                  inggris
              @elseif($nationality === "3")
                  arab
              @elseif($nationality === "4")
                  italia
              @elseif($nationality === "5")
                  prindapan
              @endif
          </p>
          <p>tentang kamu: {{$biodata}}</p>
          <p>kamu menggunakan bahasa: {{$bahasa}}</p>
      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        Footer
      </div>
      <!-- /.card-footer-->
    </div>
    <!-- /.card -->

  </section>
@endsection
