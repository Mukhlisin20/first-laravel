@extends('layout.layout')

@section('judul')
    Register disini
@endsection

@section('page')
    form
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h1 class="card-title">MaulCompany</h1>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
            <div class="content">
                <h1>Buat Account Baru!</h1>
                <form action="/home" method="post">
                    @csrf
                    <div class='form-group'>
                        <label>Fist name:</label><br>
                        <input type="text" name="firstname" placeholder="firstname">
                    </div>
                    <div class='form-group' >
                        <label>Last name:</label><br>
                        <input type="text" name="lastname" placeholder="lastname">
                    </div>
                    <br>
                    <div class='form-group'>
                        <label for="">Gender:</label><br>
                        <input type="radio" name="gender" value="1">Male<br>
                        <input type="radio" name="gender" value="2">Female<br>
                        <input type="radio" name="gender" value="">Other
                    </div>
                    <br>
                    <div class='form-group'>
                        <label for="">Nationality:</label><br>
                        <select name="nationality">
                            <option value="1">indonesia</option>
                            <option value="2">inggris</option>
                            <option value="3">arab</option>
                            <option value="4">italia</option>
                            <option value="5">prindapan</option>
                        </select>
                    </div>
                    <br>
                    <div class='form-group'>
                        <label for="">Language Spoken:</label><br>
                        <input type="checkbox" name="bahasa1" value="indonesia">Bahasa indonesia<br>
                        <input type="checkbox" name="bahasa2" value="english">English <br>
                        <input type="checkbox" name="bahasa3" value="other">Other <br>
                    </div>
                    <br>
                    <div class='form-group'>
                        <label for="">Bio:</label><br>
                        <textarea name="bio" cols="30" rows="10" placeholder="masukkan bio data anda disini!!"></textarea>
                    </div>
                    <div class='form-group'>
                        <input type="submit" value="submit">
                    </div>
                </form>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          Footer
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
@endsection