@extends('layout.layout')

@section('judul')
    Halaman Utama
@endsection

@section('page')
    dashboard
@endsection

@section('card-header')
  <h1 class="card-title">MaulCompany</h1>
@endsection

@section('content')
            <div class="content">
                <h3>Sosial Media Developer Santai Berkualitas</h3>
                <p>Belajar dan Berbagi agar hudup ini semakin santai dan berkualitas</p>
                <h4>Benefit Join di MaulCompany</h4>
                <ul>
                    <li>Mendapatkan pengalaman bekerja</li>
                    <li>Belajar sambil bermain</li>
                    <li>Bebas melakukan apapun asalkan tidak menyimpang dari aqidah</li>
                </ul>
                <h4>Cara bergabung ke MaulCompany</h4>
                <ol>
                    <li>Mengunjungi webside ini</li>
                    <li>mendaftar <a href="/form">di sini</a></li>
                    <li>Selesai</li>
                </ol>
            </div>  

    </section>
@endsection