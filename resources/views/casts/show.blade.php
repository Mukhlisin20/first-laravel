@extends('layout.layout')

@section('judul')
    Detail
@endsection

@section('page')
    detail
@endsection

@section('card-header')
    <h3 class="card-title">{{$cast->nama}}</h3>
@endsection

@section('content')
    <p class="card-text">Umur: {{$cast->umur}} tahun</p>
    <p class="card-text">Biodata: {{$cast->bio}} </p>
@endsection