@extends('layout.layout')

@section('judul')
    Edit Data
@endsection

@section('content')
            <div class="row">
                <div class="col-md-6">
                    <form action="/cast/{{$cast->id}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                          <label for="inputNama">Nama</label>
                          <input type="text" name="nama" class="form-control" id="inputNama" value="{{$cast->nama}}">
                          @error('nama')
                              <div class="alert alert-danger">
                                {{$message}}
                              </div>
                          @enderror
                        </div>
                        <div class="form-group">
                          <label for="inputUmur">Umur</label>
                          <input type="number" name="umur" class="form-control" id="inputUmur" value="{{$cast->umur}}">
                          @error('umur')
                              <div class="alert alert-danger">
                                {{$message}}
                              </div>
                          @enderror
                        </div>
                        <div class="form-group">
                            <label for="inputBio">Biodata</label>
                            <textarea name="bio" class="form-control" id="inputBio" cols="30" rows="10">{{$cast->bio}}</textarea>
                            @error('bio')
                                <div class="alert alert-danger">
                                  {{$message}}
                                </div>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                      </form>
                </div>
            </div>
@endsection