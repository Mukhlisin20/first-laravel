@extends('layout.layout')

@section('card-header')
  <a href="/cast/create" class="btn btn-primary">Tambah</a>
@endsection

@section('content')
            <table class="table">
                <thead class="thead-light">
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Nama</th>
                      <th scope="col">Umur</th>
                      <th scope="col">action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($cast as $key=>$value)
                    <tr>
                        <th scope="row"> {{$key + 1}} </th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}} tahun</td>
                        <td>
                          <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                          <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                          <form action="/cast/{{$value->id}}" method="POST">
                              @csrf
                              @method('DELETE')
                              <input type="submit" class="btn btn-danger my-1" value="Delete">
                          </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
@endsection