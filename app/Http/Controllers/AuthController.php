<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('tugas-1.form');
    }

    public function kirim(Request $request)
    {
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $biodata = $request['bio'];
        $bahasa = $request['bahasa'];

        return view('tugas-1.home', 
        [
            'firstname' => $firstname,
            'lastname' => $lastname, 
            'gender' => $gender, 
            'nationality' => $nationality,
            'biodata' => $biodata,
            'bahasa' => $bahasa
        ]);
    }
}
